﻿using System;
using AssemblyCSharp;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class Parkiranje : MonoBehaviour {
    private const int TOTAL_SKUPLJENE_TOCKE = 4;
    private int skupljeneTocke = 0;
    private static int score; 
    private bool gameOver;
    private string wonGameFormat = "YOU WIN!\nScore: {0}. Press 'R' to restart.";

    [SerializeField]
    private CarController carController;

    // Use this for initialization
    void Start () {
		
	}

    void OnEnable()
    {
        GetComponent<MeshCollider>().enabled = true;
    }

    void Update()
    {
        var speed = carController.CurrentSpeed;
        
            if (skupljeneTocke == TOTAL_SKUPLJENE_TOCKE && speed.isWithin(1f))
            {
                transform.gameObject.GetComponent<Renderer>().material.color = Color.green;
                gameOver = false;
                GetComponent<MeshCollider>().enabled = false;
                skupljeneTocke = 0;
                GameController.Instance.OnLevelComplete();
            }
			
        if (gameOver)
        {
            Utils.waitRestart();
        }
    }

    void OnGUI()
    {
        if (gameOver)
        {
            Utils.showCenteredMessage(
                string.Format(
                    wonGameFormat,
                    carController.GetComponent<ScoreCounter>().curScore));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (Utils.isPlayer(other.gameObject))
        {
            skupljeneTocke++;
			var speed = carController.CurrentSpeed;
        
            if (skupljeneTocke == TOTAL_SKUPLJENE_TOCKE && speed.isWithin(1f))
            {
                transform.gameObject.GetComponent<Renderer>().material.color = Color.green;
                gameOver = false;
                GetComponent<MeshCollider>().enabled = false;
                skupljeneTocke = 0;
                GameController.Instance.OnLevelComplete();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (Utils.isPlayer(other.gameObject))
        {
            skupljeneTocke--;
            transform.gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
