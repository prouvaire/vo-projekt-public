﻿using System;
using UnityEngine;
using AssemblyCSharp;

public class ScoreCounter : MonoBehaviour
{
    private const int ScoreLossOnCollision = 5;

    public int curScore = 100;
    public int maxScore = 100;

    private float barLength;
    private bool gameOver;

    void Start()
    {
        barLength = Screen.width / 2;
    }

    void Update()
    {
        AdjustScore(0);
        if (gameOver)
        {
            Utils.waitRestart();
        }
		if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    void OnGUI()
    {
        GUI.Box(new Rect(10, 10, barLength, 20), curScore + "");

        if (gameOver)
        {
            Utils.showCenteredMessage(
                String.Format("Score: {0}. Press 'R' to restart.", this.curScore));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (Utils.shouldLosePointsOnCollision(collision.gameObject))
        {
            AdjustScore(-ScoreLossOnCollision);
        }
    }

    private void AdjustScore(int adj)
    {
        curScore = Math.Min(Math.Max(0, curScore + adj), maxScore);

        if (curScore == 0)
        {
            gameOver = true;
        }

        barLength = (Screen.width / 2) * (curScore / (float)maxScore);
    }
}