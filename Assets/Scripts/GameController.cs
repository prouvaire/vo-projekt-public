﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    [SerializeField] private Transform _obstacleParent;
    [SerializeField] private GameObject[] _carPool;
    [SerializeField] private int[] _carCount;
    [Header("Parking space controller")]
    [SerializeField] private ParkingSpaceController _spaceController;
    [SerializeField] private Transform _car;

    private int _levelIndex;
    private int _beforeOccupiedFieldIdx;
    private Vector3 _initPosition;
    private Quaternion _initRotation;

	void Start ()
	{
	    _initPosition = _car.position;
	    _initRotation = _car.rotation;
	    Instance = this;
	    _levelIndex = 0;
	    _beforeOccupiedFieldIdx = -1;
	}
	
	void Update () {

	    if (Input.GetKeyDown(KeyCode.Alpha1))
	    {
            InitLevel(_carCount[_levelIndex]);
	    }

    }

    public void StartTheGame()
    {
        InitLevel(_carCount[_levelIndex]);
    }

    public void InitLevel(int numberOfCars)
    {
        if (numberOfCars > _spaceController._parkingSpaces.Length - 1)
        {
            Debug.Log("Too many cars");
            return;
        }
        //find available places
        DestroyLevel();
        InformationCanvasController.Instance.PushMessage("LEVEL - " + (_levelIndex+1).ToString());
	    StartCoroutine(DelayedInitLevel(1f, numberOfCars));

    }

    IEnumerator DelayedInitLevel(float delayTime, int numberOfCars)
    {
        yield return new WaitForSeconds(delayTime);

        _car.position = _initPosition;
        _car.rotation = _initRotation;
        _car.GetComponent<Rigidbody>().velocity = Vector3.zero;

        yield return null;

        var newOccupiedField = Random.Range(0, _spaceController._parkingSpaces.Length);
        if (_beforeOccupiedFieldIdx != -1)
        {
            while (newOccupiedField == _beforeOccupiedFieldIdx)
            {
                newOccupiedField = Random.Range(0, _spaceController._parkingSpaces.Length);
            }
        }

        _spaceController.OpenParkingSpace(newOccupiedField);
        _beforeOccupiedFieldIdx = newOccupiedField;

        HashSet<int> _randomPlaces = new HashSet<int>();

        while (_randomPlaces.Count < numberOfCars)
        {
            int occupation = Random.Range(0, _spaceController._parkingSpaces.Length);
            if (occupation == _spaceController._occupiedIndex)
                continue;
            _randomPlaces.Add(occupation);
        }

        foreach (var index in _randomPlaces)
        {
            Transform instantiatedCar = Instantiate(_carPool[Random.Range(0, _carPool.Length)],
                _spaceController._parkingSpaces[index].transform.position,
                _spaceController._parkingSpaces[index].transform.rotation,
                _obstacleParent).GetComponent<Transform>();
        }
    }

    void DestroyLevel()
    {
        Debug.Log(_obstacleParent.childCount);

        int safe = _obstacleParent.childCount;
        foreach (Transform child in _obstacleParent)
        {
            Destroy(child.gameObject);
        }
    }

    public void OnLevelComplete()
    {
        //add points
        //restore some of the health
        //instantiate new level
        _levelIndex++;

        

        if (_levelIndex > _carCount.Length-1)
        {
            Debug.Log("Game Is over");
            DestroyLevel();
            InformationCanvasController.Instance.PushMessage("WELL DONE");
            return;
        }
        InitLevel(_carCount[_levelIndex]);
	 
    }
}
