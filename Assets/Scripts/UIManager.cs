﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

	public bool gameStarted { get; set; }
	[SerializeField]
	private GameObject mainMenu;
    
	void Start () {
		Time.timeScale = 0;
	}
	
	void Update () {
	}

	void EnterGame()
	{
		mainMenu.SetActive(false);
		gameStarted = true;
	}
}
