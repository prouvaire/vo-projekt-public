﻿using UnityEngine;

public class LoadSceneOnClick : MonoBehaviour
{
	[SerializeField] private GameObject mainMenu;
    
	public void LoadByIndex(int mapIndex)
	{
		mainMenu.SetActive(false);
		Time.timeScale = 1;
	}
	
	public void QuitGame () {
 Application.Quit ();
 }
}
