﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AssemblyCSharp
{
    public static class Utils
    {
        const float EPSILON = 1e-6F;

        public static bool isPlayer(GameObject gameObject)
        {
            return gameObject.tag.Contains("Player");
        }

        public static bool shouldLosePointsOnCollision(GameObject gameObject)
        {
            return gameObject.tag.Contains("wall");
        }

        public static bool isWithin(this float f, float other)
        {
            return Math.Abs(f) < other;
        }

        public static bool isWithinEpsilon(this float f)
        {
            return Math.Abs(f) < EPSILON;
        }

        public static void showCenteredMessage(string message)
        {
            GUIStyle style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;
            GUI.Label(
                centeredRect(0.5f, 0.2f),
                message,
                style);
        }

        private static Rect centeredRect(float w, float h)
        {
            Rect rect = new Rect();
            rect.x = (Screen.width * (1 - w)) / 2;
            rect.y = (Screen.height * (1 - h)) / 2;
            rect.width = Screen.width * w;
            rect.height = Screen.height * h;
            return rect;
        }

        public static void waitRestart()
        {
            Time.timeScale = 0;
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);
                Time.timeScale = 1;
            }
        }
    }
}
