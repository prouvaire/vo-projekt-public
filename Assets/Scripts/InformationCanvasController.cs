﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationCanvasController : MonoBehaviour
{

    public static InformationCanvasController Instance;

    [SerializeField] private Text _headerText;

    private CanvasGroup _selfCG;

    private bool _isRunning = false;
	// Use this for initialization
	void Start ()
	{
	    Instance = this;
	    _selfCG = GetComponent<CanvasGroup>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PushMessage(string message)
    {
        _selfCG.alpha = 0;
        _headerText.text = message;

        if(_isRunning)
            return;

        StartCoroutine(ShowCanvas(0.75f, 2f));

    }

    IEnumerator ShowCanvas(float transitionTime, float duration)
    {
        float currentTime = 0;
        while (currentTime < transitionTime)
        {
            _selfCG.alpha = Mathf.Lerp(0f, 1f, currentTime / transitionTime);
            currentTime += Time.deltaTime;
            yield return null;
        }

        _selfCG.alpha = 1f;

        yield return new WaitForSeconds(duration);

        currentTime = 0;
        while (currentTime < transitionTime)
        {
            _selfCG.alpha = Mathf.Lerp(1f, 0f, currentTime / transitionTime);
            currentTime += Time.deltaTime;
            yield return null;
        }

        _selfCG.alpha = 0f;

        yield break;
    }
}
