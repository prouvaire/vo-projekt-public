﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingSpaceController : MonoBehaviour
{

    [SerializeField] public Parkiranje[] _parkingSpaces;
    [SerializeField] public int _occupiedIndex = 0;

	void Start ()
	{
	    _parkingSpaces = GetComponentsInChildren<Parkiranje>(true);
        CloseAllParkingSpaces();
	}
	
	void Update () {
	}

    void CloseAllParkingSpaces()
    {
        foreach (var space in _parkingSpaces)
        {
            space.gameObject.SetActive(false);
        }
    }

    public void OpenParkingSpace(int index)
    {
        _occupiedIndex = index;
        for (int i = 0; i < _parkingSpaces.Length; i++)
        {
            if (i == index)
            {
                _parkingSpaces[i].gameObject.SetActive(true);
                _parkingSpaces[i].GetComponent<Renderer>().material.color = Color.white;
            }
            else
            {
                _parkingSpaces[i].gameObject.SetActive(false);
            }
        }
    }
}
